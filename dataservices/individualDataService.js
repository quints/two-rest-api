var dataStore = require("./dbConnManager");
const individualsDocType = "inds";
exports.prepareIndividualForInsert = function(individualId, individualName, individualEmail){
    var obj = {};
    obj._id = individualId;
    obj.indName = individualName;
    obj.indEmail = individualEmail;
    obj.doctype = individualsDocType;

    return obj;
}


exports.insert = function(individualToInsert){
    //insert log
    dataStore.db.insert(individualToInsert, dataStore.defaultDbErrorListener);
}

