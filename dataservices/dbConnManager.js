const dbName = "smsdb";
const dbServerUrl = "http://dbadmin:dhakal@localhost:5984";
const nano = require("nano")(dbServerUrl);

var dbCallbackFn = function(err, body, header){
    if(err){
        console.log("oops error occured");
    }
    else{
        console.log("success!");
    }

}

var db = nano.use(dbName, dbCallbackFn);

exports.db = db;
exports.defaultDbErrorListener = dbCallbackFn;