var dataStore = require("./dbConnManager");
const groupsDocType = "groups";
exports.prepareGroupForInsert = function(groupId, groupName, email){
    var obj = {};
    obj._id = groupId;
    obj.groupName = groupName;
    obj.email = email;
    obj.doctype = groupsDocType;

    return obj;
}


exports.insert = function(groupToInsert){
    //insert log
    dataStore.db.insert(groupToInsert, dataStore.defaultDbErrorListener);
}

