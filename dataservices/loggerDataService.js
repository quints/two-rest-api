var dataStore = require("./dbConnManager");
const logsDocType = "logs";
exports.prepareLogForInsert = function(logType, logData, req){
    //prepare log object
    var obj = {};
    obj.logData = logData;
    obj.doctype = logsDocType;
    obj.logSource = "rest-api";
    obj.logType = logType;
    obj.req = req;
    obj.req = {};
    obj.req.headers = req.headers;
    obj.req.url = req.url;
    obj.req.httpVersion = req.httpVersion;
    obj.req.httpMethod = req.method;

    return obj;
}

exports.insert = function(logToInsert){
    dataStore.db.insert(logToInsert, dataStore.defaultDbErrorListener);
}