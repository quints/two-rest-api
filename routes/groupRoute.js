const uuidV1 = require('uuid/v1');
const express = require("express");

const groupService = require('../services/groupService');
const loggerService = require("../services/loggerService");

var groupRouter = express.Router();
groupRouter.post("/", function(req, res, next){
    //register a group

    var groupId = uuidV1();;

    var groupName = req.body.groupName;
    var groupEmail = req.body.groupEmail;

    //come on, where is async version?? where is promise???
    groupService.createGroup(groupId, groupName, groupEmail);
    
    //if successful, 
    loggerService.info({docId: groupId, 'what': 'inserted group'}, req);

    res.status(201).send({id : groupId});
});
module.exports = groupRouter;