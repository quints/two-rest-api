const uuidV1 = require('uuid/v1');
const express = require("express");

const individualService = require('../services/individualService');
const loggerService = require("../services/loggerService");

var individualRouter = express.Router();
individualRouter.post("/", function(req, res, next){
    //register an individual

    var individualId = uuidV1();;

    var name = req.body.myName;
    var email = req.body.myEmail;

    //come on, where is async version?? where is promise???
    individualService.createIndividual(individualId, name, email);
    
    //if successful, 
    loggerService.info({docId: individualId, 'what': 'inserted individual'}, req);

    res.status(201).send({id : individualId});
});
module.exports = individualRouter;