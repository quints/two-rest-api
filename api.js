const express = require("express");
const bodyParser = require("body-parser");

const loggerService = require("./services/loggerService");

var restApi = express();


//body parser
//restApi.use(bodyParser.urlencoded({extended: true}));
restApi.use(bodyParser.json());

//cors
var cors = require('cors');
var corsOptions = {
  origin: 'http://localhost:3026',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
restApi.use(cors(corsOptions));

//global express error handler
restApi.use(function(err, req, res, next){
    loggerService.err(err, req);
    res.status(500).send("something broke!!!");
});

const groupRouter = require("./routes/groupRoute");
restApi.use("/grp", groupRouter);

const individualRouter = require("./routes/individualRoute");
restApi.use("/ind", individualRouter);

restApi.listen(3013);