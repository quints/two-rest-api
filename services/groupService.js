
var groupDataService = require("../dataservices/groupDataService");

exports.createGroup = function(groupId, groupName, groupEmail){
    //validate oname, email for uniqueness, other validations
    

    //validation done, lets prep obj
    var groupToInsert = groupDataService.prepareGroupForInsert(groupId, groupName, groupEmail);
    groupDataService.insert(groupToInsert);    
}