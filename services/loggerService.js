
var loggerDataService = require("../dataservices/loggerDataService");

var writeLogSync = function(logType, logData, req){
    var logToInsert = loggerDataService.prepareLogForInsert(logType, logData, req);
    loggerDataService.insert(logToInsert);
}


exports.err = function(logData, req){
    writeLogSync("err", logData, req);
}

exports.info = function(logData, req){
    writeLogSync("info", logData, req);
}