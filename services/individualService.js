
var individualDataService = require("../dataservices/individualDataService");

exports.createIndividual = function(individualId, individualName, individualEmail){
    //validate name, email for uniqueness, other validations
    

    //validation done, lets prep obj
    var individualToInsert = individualDataService.prepareIndividualForInsert(individualId, individualName, individualEmail);
    individualDataService.insert(individualToInsert);    
}
